﻿using FluentAssertions;
using HaveBoxJSON;
using System;
using Xunit;

namespace HaveBoxJSONUnitTets
{
    public class SerializeTests
    {
        public readonly JsonConverter _converter;

        public SerializeTests()
        {
            _converter = new JsonConverter();
        }

        [Fact]
        public void Given_A_Dto_When_Serializing_The_The_Result_Json_Has_The_Same_Values_As_Dto()
        {
            var person = new Person { Date = DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"), GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"), Id = 123, Name = "John Doe", IsHappy = true, Balance = -42.01m, Address = new Address { Street = "The Street" } };

            var json = _converter.Serialize(person);

            json.Should().Be("{\"Date\":\"2013-11-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-42.01,\"Address\":{\"Street\":\"The Street\"}}");
        }

        [Fact]
        public void Given_A_Default_Dto_When_Serializing_The_The_Result_Json_Has_The_Same_Values_As_Dto()
        {
            var person = new Person();

            var json = _converter.Serialize(person);

            json.Should().Be("{\"Date\":\"0001-01-01T00:00:00.0000000+01:00\",\"GlobalId\":\"00000000-0000-0000-0000-000000000000\",\"Id\":0,\"Name\":null,\"IsHappy\":false,\"Balance\":0,\"Address\":null}");
        }

        [Fact]
        public void Given_An_Uninitialized_Dto_When_Serializing_The_The_Result_Json_Is_Empty()
        {
            Person person = null;

            var json = _converter.Serialize(person);

            json.Should().Be("{}");
        }

        [Fact]
        public void Given_A_String_Property_With_Escape_Charaters_When_Serializing_Then_Object_Is_Serialized()
        {
            var escape = new Escape { escapedString = "\" \\ / \b \f \n \r \t", };

            var json = _converter.Serialize(escape);

            json.Should().Be("{\"escapedString\":\"\\\" \\\\ \\/ \\b \\f \\n \\r \\t\"}");
        }

        public class Escape
        {
            public string escapedString { get; set; }
        }

        public class Person
        {
            public DateTime Date { get; set; }
            public Guid GlobalId { get; set; }
            public int Id { get; set; }
            public string Name { get; set; }
            public bool IsHappy { get; set; }
            public decimal Balance { get; set; }
            public Address Address { get; set; }
        }

        public class Address
        {
            public string Street { get; set; }
        }
    }
}
