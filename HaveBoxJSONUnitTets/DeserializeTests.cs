﻿using FluentAssertions;
using HaveBoxJSON;
using System;
using System.Collections.Generic;
using Xunit;

namespace HaveBoxJSONUnitTets
{
    public class DeserializeTests
    {
        public readonly JsonConverter _converter;

        public DeserializeTests()
        {
            _converter = new JsonConverter();
        }

        [Fact]
        public void Given_A_Person_Json_Document_When_Deserializing_Then_An_Instance_Of_Person_Is_Returned()
        {
            var personDto = CreateAndSetupPersonDto();
            var person = "{\"Balance\":-42.01,\"Id\":123,\"Name\":  \"John \\\"Unknown\\\" Doe\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Date\":\"2013-11-27T22:43:37.5065305+01:00\",\"IsHappy\":true,\"Kids\":[\"Ringler\",\"Dingler\"],\"PhoneNumbers\":{\"Home\":\"1234\",\"work\":\"5678\"}, \"Address\":{\"Street\":\"The Street\",\"City\":{\"Zip\":90210,\"CityName\":\"Beverly Hills\"}}}";

            var personDtoFromJson = _converter.Deserialize<Person>(person);

            personDtoFromJson.ShouldBeEquivalentTo(personDto);
        }

        [Fact]
        public void Given_A_String_With_Escaped_Character_When_Deserializing_Then_The_String_Is_Deserialized()
        {
            var escapeDto = CreateEscapeDto();

            var result = _converter.Deserialize<Escape>("{\"escapedString\" : \"\\\" \\\\ \\/ \\b \\f \\n \\r \\t \\u0048\"}");

            result.ShouldBeEquivalentTo(escapeDto);
        }

        private Escape CreateEscapeDto()
        {
            return new Escape
            {
                escapedString = "\" \\ / \b \f \n \r \t \u0048",
            };
        }

        public class Escape
        {
            public string escapedString { get; set; }
        }

        public Person CreateAndSetupPersonDto()
        {
            return new Person
            {
                GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"),
                Id = 123,
                Name = "John \"Unknown\" Doe",
                IsHappy = true,
                Balance = -42.01m,
                Date = DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"),
                Kids = new List<string> {"Ringler", "Dingler"},
                Address = new Address 
                {
                    Street = "The Street",
                    City = new City 
                    { 
                        CityName = "Beverly Hills",
                        Zip = 90210,
                    }
                },
                PhoneNumbers = new Dictionary<string, string> { 
                    { "Home", "1234" },
                    { "work", "5678" },
                }
            };
        }

        public class Person
        {
            public DateTime Date { get; set; }
            public Guid GlobalId { get; set; }
            public int Id { get; set; }
            public string Name { get; set; }
            public bool IsHappy { get; set; }
            public IList<string> Kids { get; set; }
            public decimal Balance { get; set; }
            public Address Address { get; set; }
            public Dictionary<string, string> PhoneNumbers { get; set; }
        }

        public class Address
        {
            public string Street { get; set; }
            public City City { get; set; }
        }

        public class City
        {
            public int Zip { get; set; }
            public string CityName { get; set; }
        }
    }
}
