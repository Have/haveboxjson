﻿using FluentAssertions;
using HaveBoxJSON;
using System;
using System.Collections.Generic;
using Xunit;

namespace HaveBoxJSONUnitTets
{
    public class SerializeDeserializeTests
    {
        public readonly JsonConverter _converter;

        public SerializeDeserializeTests()
        {
            _converter = new JsonConverter();
        }

        [Fact]
        public void Given_A_Dto_When_Serializing_And_Deserializing_Then_The_Result_Matches_The_Dto()
        {
            var personDto = CreateAndSetupPersonDto();

            var result = _converter.Deserialize<Person>(_converter.Serialize(personDto));

            result.ShouldBeEquivalentTo(personDto);
        }

        public Person CreateAndSetupPersonDto()
        {
            return new Person
            {
                GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"),
                Id = 123,
                Name = "John \"Unknown\" Doe",
                IsHappy = true,
                Balance = -42.01m,
                Date = DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"),
                Kids = new List<string> { "Ringler", "Dingler" },
                Address = new Address
                {
                    Street = "The Street",
                    City = new City
                    {
                        CityName = "Beverly Hills",
                        Zip = 90210,
                    }
                },
                PhoneNumbers = new Dictionary<string, string> { 
                    { "Home", "1234" },
                    { "work", "5678" },
                }
            };
        }

        public class Person
        {
            public DateTime Date { get; set; }
            public Guid GlobalId { get; set; }
            public int Id { get; set; }
            public string Name { get; set; }
            public bool IsHappy { get; set; }
            public IList<string> Kids { get; set; }
            public decimal Balance { get; set; }
            public Address Address { get; set; }
            public IDictionary<string, string> PhoneNumbers { get; set; }
        }

        public class Address
        {
            public string Street { get; set; }
            public City City { get; set; }
        }

        public class City
        {
            public int Zip { get; set; }
            public string CityName { get; set; }
        }
    }
}
