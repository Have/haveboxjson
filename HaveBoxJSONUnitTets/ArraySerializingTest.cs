﻿using FluentAssertions;
using HaveBoxJSON;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xunit.Extensions;

namespace HaveBoxJSONUnitTets
{
    public class ArraySerializingTest
    {
        public readonly JsonConverter _converter;

        public ArraySerializingTest()
        {
            _converter = new JsonConverter();
        }

        [Theory]
        [PropertyData("InputOutputData")]
        public void Given_Object_Array_When_Serializing_Then_A_Json_Document_Is_Returned(object array, string result)
        {
            var json = _converter.Serialize(array);

            json.Should().Be(result);            
        }

        public static IEnumerable<object[]> InputOutputData
        {
            get
            {
                return new[]
                {
                    new object[] { new char[] { 'A','B','C' }, "[\"A\",\"B\",\"C\"]" },

                    new object[] { new[] { "ABC", "DEF", "GHI", "\" \\ \f" }, "[\"ABC\",\"DEF\",\"GHI\",\"\\\" \\\\ \\f\"]" },

                    new object[] { new byte[] { 10, 20, 30 }, "[ChQe]" },
                    new object[] { new sbyte[] { 10, 20, 30 }, "[10,20,30]" },

                    new object[] { new int[] { 42, 106, 27839 }, "[42,106,27839]" },
                    new object[] { new short[] { 42, 106, 27839 }, "[42,106,27839]" },
                    new object[] { new long[] { 42, 106, 27839 }, "[42,106,27839]" },
                    new object[] { new uint[] { 42, 106, 27839 }, "[42,106,27839]" },
                    new object[] { new ushort[] { 42, 106, 27839 }, "[42,106,27839]" },
                    new object[] { new ulong[] { 42, 106, 27839 }, "[42,106,27839]" },

                    new object[] { new float[] { 42.42f, 106.99f, 27839.21f }, "[42.42,106.99,27839.21]" },
                    new object[] { new double[] { 42.42, 106.99, 27839.21 }, "[42.42,106.99,27839.21]" },
                    new object[] { new decimal[] { 42.42m, 106.99m, 27839.21m }, "[42.42,106.99,27839.21]" },

                    new object[] { new[] { new Guid("06B9333B-322B-4AF1-B600-2E49D425ACB7"), new Guid("B0D23DCF-315F-4DDC-B347-07CF299373F4"), new Guid("1640CAD6-095D-4C62-B1E5-64E9CCD7DF83") }, "[\"06b9333b-322b-4af1-b600-2e49d425acb7\",\"b0d23dcf-315f-4ddc-b347-07cf299373f4\",\"1640cad6-095d-4c62-b1e5-64e9ccd7df83\"]" },
                    new object[] { new[] { DateTime.Parse("2013-12-27T22:43:37.5065305+01:00"), DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"), DateTime.Parse("2013-10-27T22:43:37.5065305+01:00") }, "[\"2013-12-27T22:43:37.5065305+01:00\",\"2013-11-27T22:43:37.5065305+01:00\",\"2013-10-27T22:43:37.5065305+01:00\"]" },

                    new object[] { new[] { true, false, true }, "[true,false,true]" },

                    new object[] { new[] { new Person { Date = DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"), GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"), Id = 123, Name = "John Doe", IsHappy = true, Balance = -42.01m, Address = new Address { Street = "The Street" } },
                                  new Person { Date = DateTime.Parse("2013-12-27T22:43:37.5065305+01:00"), GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"), Id = 123, Name = "John Doe", IsHappy = true, Balance = -45.01m, Address = new Address { Street = "The Street" } }}, "[{\"Date\":\"2013-11-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-42.01,\"Address\":{\"Street\":\"The Street\"}},{\"Date\":\"2013-12-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-45.01,\"Address\":{\"Street\":\"The Street\"}}]" },

                    new object[] { new[] { new[] { "ABC", "DEF", "GHI" }, new[] { "JKL", "MNO" } }, "[[\"ABC\",\"DEF\",\"GHI\"],[\"JKL\",\"MNO\"]]" },

                    new object[] { new dynamic[] { "ABC", 123, -43.78, new Guid("06B9333B-322B-4AF1-B600-2E49D425ACB7"), new Person { Date = DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"), GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"), Id = 123, Name = "John Doe", IsHappy = true, Balance = -42.01m, Address = new Address { Street = "The Street" }} }, "[\"ABC\",123,-43.78,\"06b9333b-322b-4af1-b600-2e49d425acb7\",{\"Date\":\"2013-11-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-42.01,\"Address\":{\"Street\":\"The Street\"}}]" },
                    new object[] { new object[] { "ABC", 123, -43.78, new Guid("06B9333B-322B-4AF1-B600-2E49D425ACB7"), new Person { Date = DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"), GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"), Id = 123, Name = "John Doe", IsHappy = true, Balance = -42.01m, Address = new Address { Street = "The Street" }} }, "[\"ABC\",123,-43.78,\"06b9333b-322b-4af1-b600-2e49d425acb7\",{\"Date\":\"2013-11-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-42.01,\"Address\":{\"Street\":\"The Street\"}}]" },

                    new object[] { new List<string> { "ABC", "DEF", "GHI" }, "[\"ABC\",\"DEF\",\"GHI\"]" },
                    new object[] { new Collection<string> { "ABC", "DEF", "GHI" }, "[\"ABC\",\"DEF\",\"GHI\"]" },

                    new object[] { new List<int> { 42, 106, 27839 }, "[42,106,27839]" },
                    new object[] { new Collection<int> { 42, 106, 27839 }, "[42,106,27839]" },

                    new object[] { new Dictionary<string, int> { {"Key1", 42}, {"Key2", 106}, {"Key3", 27839} }, "{\"Key1\":42,\"Key2\":106,\"Key3\":27839}" },
                    new object[] { new Dictionary<string, string> { {"Key1", "abc"}, {"Key2", "def"}, {"Key3", "ghi"} }, "{\"Key1\":\"abc\",\"Key2\":\"def\",\"Key3\":\"ghi\"}" },

                    new object[] { new Dictionary<string, Person> { { "Key1", new Person { Date = DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"), GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"), Id = 123, Name = "John Doe", IsHappy = true, Balance = -42.01m, Address = new Address { Street = "The Street" } } } }, "{\"Key1\":{\"Date\":\"2013-11-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-42.01,\"Address\":{\"Street\":\"The Street\"}}}" },                
                };
            }
        }

        public class Person
        {
            public DateTime Date { get; set; }
            public Guid GlobalId { get; set; }
            public int Id { get; set; }
            public string Name { get; set; }
            public bool IsHappy { get; set; }
            public decimal Balance { get; set; }
            public Address Address { get; set; }
        }

        public class Address
        {
            public string Street { get; set; }
        }
    }
}
