﻿using FluentAssertions;
using HaveBoxJSON;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xunit.Extensions;

namespace HaveBoxJSONUnitTets
{
    public class ArrayDeserializingTests
    {
        public readonly JsonConverter _converter;

        public ArrayDeserializingTests()
        {
            _converter = new JsonConverter();
        }

        [Theory]
        [PropertyData("InputOutputData")]
        public void Given_A_Json_Document_When_Deserializing_Then_An_Array_Is_Returned(string json, object array)
        {
            var result = _converter.Deserialize(array.GetType(), json);

            result.ShouldBeEquivalentTo(array);
        }

        public static IEnumerable<object[]> InputOutputData
        {
            get
            {
                return new[]
                {
                    new object[] { "[\"ABC\",\"DEF\",\"GHI\"]", new[] { "ABC", "DEF", "GHI" } },

                    new object[] { "[42,106,27839]", new int[] { 42, 106, 27839 } },
                    new object[] { "[42,106,27839]", new short[] { 42, 106, 27839 } },
                    new object[] { "[42,106,27839]", new long[] { 42, 106, 27839 } },

                    new object[] { "[42,106,27839]", new uint[] { 42, 106, 27839 } },
                    new object[] { "[42,106,27839]", new ushort[] { 42, 106, 27839 } },
                    new object[] { "[42,106,27839]", new ulong[] { 42, 106, 27839 } },

                    new object[] { "\"ChQe\"", new byte[] { 10, 20, 30 } },
                    new object[] { "[10,20,30]", new sbyte[] { 10, 20, 30 } },

                    new object[] { "[\"06b9333b-322b-4af1-b600-2e49d425acb7\",\"b0d23dcf-315f-4ddc-b347-07cf299373f4\",\"1640cad6-095d-4c62-b1e5-64e9ccd7df83\"]", new Guid[] { new Guid("06B9333B-322B-4AF1-B600-2E49D425ACB7"), new Guid("B0D23DCF-315F-4DDC-B347-07CF299373F4"), new Guid("1640CAD6-095D-4C62-B1E5-64E9CCD7DF83") } },
                    new object[] { "[\"2013-12-27T22:43:37.5065305+01:00\",\"2013-11-27T22:43:37.5065305+01:00\",\"2013-10-27T22:43:37.5065305+01:00\"]", new[] { DateTime.Parse("2013-12-27T22:43:37.5065305+01:00"), DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"), DateTime.Parse("2013-10-27T22:43:37.5065305+01:00") } },

                    new object[] { "[42.42,106.99,27839.21]", new float[] { 42.42f, 106.99f, 27839.21f } },
                    new object[] { "[42.42,106.99,27839.21]", new double[] { 42.42, 106.99, 27839.21 } },
                    new object[] { "[42.42,106.99,27839.21]", new decimal[] { 42.42m, 106.99m, 27839.21m } },

                    new object[] { "[true,false,true]", new[] { true, false, true } },

                    new object[] { "[\"A\",\"B\",\"C\"]", new char[] { 'A','B','C' } },

                    new object[] { "[[\"ABC\",\"DEF\",\"GHI\"],[\"JKL\",\"MNO\"]]", new[] { new[] { "ABC", "DEF", "GHI" }, new[] { "JKL", "MNO" } } },

                    new object[] { "[{\"Date\":\"2013-11-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-42.01,\"Address\":{\"Street\":\"The Street\"}},{\"Date\":\"2013-12-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-45.01,\"Address\":{\"Street\":\"The Street\"}}]", 
                        new[] { new Person { Date = DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"), GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"), Id = 123, Name = "John Doe", IsHappy = true, Balance = -42.01m, Address = new Address { Street = "The Street" } },
                                  new Person { Date = DateTime.Parse("2013-12-27T22:43:37.5065305+01:00"), GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"), Id = 123, Name = "John Doe", IsHappy = true, Balance = -45.01m, Address = new Address { Street = "The Street" } }} },

                    new object[] { "[\"ABC\",123,-43.78,\"06b9333b-322b-4af1-b600-2e49d425acb7\",[\"ABC\",\"DEF\",\"GHI\"]]", new object[] { "ABC", "123", "-43.78", "06b9333b-322b-4af1-b600-2e49d425acb7", "[\"ABC\",\"DEF\",\"GHI\"]" } },
                    new object[] { "[\"ABC\",123,-43.78,\"06b9333b-322b-4af1-b600-2e49d425acb7\",{\"Date\":\"2013-11-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-42.01,\"Address\":{\"Street\":\"The Street\"}}]", new object[] { "ABC", "123", "-43.78", "06b9333b-322b-4af1-b600-2e49d425acb7", "{\"Date\":\"2013-11-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-42.01,\"Address\":{\"Street\":\"The Street\"}}" } },

                    new object[] { "[\"ABC\",\"DEF\",\"GHI\"]", new List<string> { "ABC", "DEF", "GHI" } },
                    new object[] { "[123,456,789]", new List<int> { 123, 456, 789 } },

                    new object[] { "{\"Key1\":42,\"Key2\":106,\"Key3\":-27839}", new Dictionary<string, int> { {"Key1", 42}, {"Key2", 106}, {"Key3", -27839} } },
                    new object[] { "{\"Key1\":10,\"Key2\":15,\"Key3\":-27}", new Dictionary<string, short> { {"Key1", 10}, {"Key2", 15}, {"Key3", -27} } },
                    new object[] { "{\"Key1\":1808090,\"Key2\":8980980878,\"Key3\":-797676576}", new Dictionary<string, long> { {"Key1", 1808090}, {"Key2", 8980980878}, {"Key3", -797676576} } },

                    new object[] { "{\"Key1\":42,\"Key2\":106,\"Key3\":27839}", new Dictionary<string, uint> { {"Key1", 42}, {"Key2", 106}, {"Key3", 27839} } },
                    new object[] { "{\"Key1\":10,\"Key2\":15,\"Key3\":27}", new Dictionary<string, ushort> { {"Key1", 10}, {"Key2", 15}, {"Key3", 27} } },
                    new object[] { "{\"Key1\":1808090,\"Key2\":8980980878,\"Key3\":797676576}", new Dictionary<string, ulong> { {"Key1", 1808090}, {"Key2", 8980980878}, {"Key3", 797676576} } },                

                    new object[] { "{\"Key1\":true,\"Key2\":false,\"Key3\":true}", new Dictionary<string, bool> { {"Key1", true}, {"Key2", false}, {"Key3", true} } },

                    new object[] { "{\"Key1\":\"abc\",\"Key2\":\"def\",\"Key3\":\"ghi\"}", new Dictionary<string, string> { {"Key1", "abc"}, {"Key2", "def"}, {"Key3", "ghi"} } },                

                    new object[] { "{\"Key1\":10,\"Key2\":20,\"Key3\":30}", new Dictionary<string, byte> { {"Key1", 10}, {"Key2", 20}, {"Key3", 30} } },
                    new object[] { "{\"Key1\":-10,\"Key2\":-20,\"Key3\":-30}", new Dictionary<string, sbyte> { {"Key1", -10}, {"Key2", -20}, {"Key3", -30} } },                

                    new object[] { "{\"Key1\":10.10,\"Key2\":20.20,\"Key3\":30.30}", new Dictionary<string, float> { {"Key1", 10.10f}, {"Key2", 20.20f}, {"Key3", 30.30f} } },                
                    new object[] { "{\"Key1\":10.10,\"Key2\":20.20,\"Key3\":30.30}", new Dictionary<string, double> { {"Key1", 10.10}, {"Key2", 20.20}, {"Key3", 30.30} } },                
                    new object[] { "{\"Key1\":10.10,\"Key2\":20.20,\"Key3\":30.30}", new Dictionary<string, decimal> { {"Key1", 10.10m}, {"Key2", 20.20m}, {"Key3", 30.30m} } },
                
                    new object[] { "{\"Key1\":\"2013-12-27T22:43:37.5065305+01:00\"}", new Dictionary<string, DateTime> { {"Key1", DateTime.Parse("2013-12-27T22:43:37.5065305+01:00")},},},
                    new object[] { "{\"Key1\":\"06b9333b-322b-4af1-b600-2e49d425acb7\"}", new Dictionary<string, Guid> { {"Key1", Guid.Parse("06b9333b-322b-4af1-b600-2e49d425acb7")},},},

                    new object[] { "{\"Key1\":[123,456,789]}", new Dictionary<string, int[]> { {"Key1", new[] {123,456,789}}}},                
                    new object[] { "{\"Key1\":[123,456,789]}", new Dictionary<string, List<int>> { {"Key1", new List<int> {123,456,789}}}},
                    new object[] { "{\"Key1\":[\"ABC\",\"DEF\",\"GHI\"]}", new Dictionary<string, List<string>> { {"Key1", new List<string> { "ABC", "DEF", "GHI" }}}},                

                    new object[] { "{\"Key1\":{\"Date\":\"2013-11-27T22:43:37.5065305+01:00\",\"GlobalId\":\"c4ec0f95-002d-480a-994f-7677ee14f633\",\"Id\":123,\"Name\":\"John Doe\",\"IsHappy\":true,\"Balance\":-42.01,\"Address\":{\"Street\":\"The Street\"}}}", new Dictionary<string, Person> {{ "Key1", new Person { Date = DateTime.Parse("2013-11-27T22:43:37.5065305+01:00"), GlobalId = new Guid("c4ec0f95-002d-480a-994f-7677ee14f633"), Id = 123, Name = "John Doe", IsHappy = true, Balance = -42.01m, Address = new Address { Street = "The Street" }}}}},

                    new object[] { "{\"Key1\":{\"Key1\":42,\"Key2\":106,\"Key3\":-27839}}", new Dictionary<string, Dictionary<string, int>> { {"Key1", new Dictionary<string, int> { {"Key1", 42}, {"Key2", 106}, {"Key3", -27839} } } } },
                };
            }
        }

        public class Person
        {
            public DateTime Date { get; set; }
            public Guid GlobalId { get; set; }
            public int Id { get; set; }
            public string Name { get; set; }
            public bool IsHappy { get; set; }
            public decimal Balance { get; set; }
            public Address Address { get; set; }
        }

        public class Address
        {
            public string Street { get; set; }
        }
    }
}
