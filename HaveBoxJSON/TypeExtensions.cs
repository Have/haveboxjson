﻿using System;

namespace HaveBoxJSON
{
    internal static class TypeExtension
    {
        public static bool IsNumeric(this Type dataType)
        {
            if (dataType == null)
                throw new ArgumentNullException("dataType");

            return (   dataType == typeof(sbyte)
                    || dataType == typeof(int)
                    || dataType == typeof(long)
                    || dataType == typeof(short)
                    || dataType == typeof(byte)
                    || dataType == typeof(uint)
                    || dataType == typeof(ulong)
                    || dataType == typeof(ushort)
                    || dataType == typeof(float)
                    || dataType == typeof(double)
                    || dataType == typeof(decimal));
        }

        public static bool IsBoolean(this Type dataType)
        {
            if (dataType == null)
                throw new ArgumentNullException("dataType");

            return (dataType == typeof(bool));
        }

        public static bool IsNonNumericDataType(this Type dataType)
        {
            if (dataType == null)
                throw new ArgumentNullException("dataType");

            return (dataType == typeof(System.String)
                    || dataType == typeof(System.DateTime)
                    || dataType == typeof(System.Guid));
        }
    }
}
