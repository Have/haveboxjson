﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HaveBoxJSON
{
    internal static class IEnumerableExtensions
    {
        public static void Each<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (T item in enumeration)
            {
                action(item);
            }
        }

        public static bool HasIDictionaryInterface(this Type type)
        {
            return type.GetInterfaces().Where(x => x.Name.Contains("IDictionary`2") && x.Namespace == "System.Collections.Generic").Any();
        }

        public static bool IsIDictionaryInterface(this Type type)
        {
            return type.Name.Contains("IDictionary`2") && type.Namespace == "System.Collections.Generic";
        }

        public static bool HasIEnumerableInterface(this Type type)
        {
            return type.GetInterfaces().Where(x => x.Name.Contains("IEnumerable`1") && x.Namespace == "System.Collections.Generic").Any();
        }
    }
}
