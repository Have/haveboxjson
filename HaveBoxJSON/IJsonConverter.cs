﻿using System;

namespace HaveBoxJSON
{
    interface IJsonConverter
    {
        string Serialize(object @object);

        T Deserialize<T>(string json);
        object Deserialize(Type type, string json);
    }
}
