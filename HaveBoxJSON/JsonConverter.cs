﻿using HaveBoxJSON.Converters;
using System;

namespace HaveBoxJSON
{
    public class JsonConverter : IJsonConverter
    {
        internal readonly JsonSerializer _jsonSerializer;
        internal readonly JsonDeserializer _jsonDeserializer;

        public JsonConverter()
        {
            _jsonSerializer = new JsonSerializer();
            _jsonDeserializer = new JsonDeserializer();
        }

        public string Serialize(object @object)
        {
            return _jsonSerializer.Serialize(@object);
        }

        public T Deserialize<T>(string json)
        {
            return _jsonDeserializer.Deserialize<T>(json);
        }

        public object Deserialize(Type type, string json)
        {
            return _jsonDeserializer.Deserialize(type, json);
        }
    }
}
