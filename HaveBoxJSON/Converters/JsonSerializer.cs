﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace HaveBoxJSON.Converters
{
    internal class JsonSerializer
    {
        private delegate string[][] GetPropertiesValues(object @object, JsonSerializer jsonSerializer, string[] qouteTemplate);
        private delegate string[] ArraySerializer(object objects);

        private Dictionary<Type, GetPropertiesValues> _calculatedProperties;
        private Dictionary<Type, ArraySerializer> _arraySerializers;
        private TimeSpan _utcOffSet = TimeZoneInfo.Local.BaseUtcOffset;

        string[] _jsonObjectTemplate;
        string[] _jsonArrayTemplate;
        string[] _qouteTemplate;

        public JsonSerializer()
        {
            _calculatedProperties = new Dictionary<Type, GetPropertiesValues>();
            _arraySerializers = SetupArraySerializers();
            _utcOffSet = TimeZoneInfo.Local.BaseUtcOffset;

            _jsonObjectTemplate = new[] { "", ":", "", "," };
            _jsonArrayTemplate = new[] { "", ","};
            _qouteTemplate = new[] { "\"", "", "\"" };
        }

        private Dictionary<Type, ArraySerializer> SetupArraySerializers()
        {
            var arraySerializers = new Dictionary<Type, ArraySerializer>();

            arraySerializers[typeof(byte[])] = ConvertBytesAndBase64EncodeToStrings;
            arraySerializers[typeof(sbyte[])] = ConvertValuesToStrings<sbyte>;

            arraySerializers[typeof(int[])] = ConvertValuesToStrings<int>;
            arraySerializers[typeof(long[])] = ConvertValuesToStrings<long>;
            arraySerializers[typeof(short[])] = ConvertValuesToStrings<short>;

            arraySerializers[typeof(uint[])] = ConvertValuesToStrings<uint>;
            arraySerializers[typeof(ulong[])] = ConvertValuesToStrings<ulong>;
            arraySerializers[typeof(ushort[])] = ConvertValuesToStrings<ushort>;

            arraySerializers[typeof(float[])] = ConvertPrecisionValuesToStrings<float>;
            arraySerializers[typeof(double[])] = ConvertPrecisionValuesToStrings<double>;
            arraySerializers[typeof(decimal[])] = ConvertPrecisionValuesToStrings<decimal>;

            arraySerializers[typeof(char[])] = ConvertValuesToQuotedStrings<char>;

            arraySerializers[typeof(string[])] = QuoteStrings;
            arraySerializers[typeof(Guid[])] = ConvertValuesToQuotedStrings<Guid>;
            arraySerializers[typeof(DateTime[])] = ConvertDateTimeToQuotedStrings;
            arraySerializers[typeof(bool[])] = ConvertBoolsToStrings;
            arraySerializers[typeof(object[])] = ConvertDynamicsToStrings;

            return arraySerializers;
        }

        public string Serialize(object @object)
        {
            if (@object == null)
            {
                return "{}";
            }

            var type = @object.GetType();
            GetPropertiesValues getPropertiesValues;
            if (!_calculatedProperties.TryGetValue(type, out getPropertiesValues))
            {
                if (type.IsArray)
                {
                    ArraySerializer arraySerializer;
                    string[] strings;
                    if (_arraySerializers.TryGetValue(type, out arraySerializer))
                    {
                        strings = arraySerializer(@object);
                    }
                    else
                    {
                        strings = ConvertObjectsToStrings(@object);
                    }
                    
                    return BuildJsonArray(strings);
                }

                if (@object is IDictionary)
                {
                    getPropertiesValues = (GetPropertiesValues)Delegate.CreateDelegate(typeof(GetPropertiesValues), this, typeof(JsonSerializer).GetMethod("ExtractDictionaryValues").MakeGenericMethod(@object.GetType().GetGenericArguments()));
                }
                else if (@object is IEnumerable)
                {
                    return Serialize(((IEnumerable)@object).Cast<object>().ToArray());
                }
                else
                {
                    getPropertiesValues = CreateGetPropertiesValues(type);
                }
                _calculatedProperties[type] = getPropertiesValues;
            }

            return BuildJsonObject(getPropertiesValues(@object, this, _qouteTemplate));
        }

        public string[][] ExtractDictionaryValues<T, U>(object @object, JsonSerializer jsonSerializer, string[] qouteTemplate)
        {
            var dictionary = (IDictionary<T,U>) @object;
            var properties = new string[dictionary.Count][];

            var index = 0;
            foreach(var keyvalue in dictionary)
            {
                properties[index] = new string[2];
                qouteTemplate[1] = keyvalue.Key.ToString();
                properties[index][0] = string.Join("", qouteTemplate, 0, 3);
                properties[index][1] = FormatObject(keyvalue.Value);

                index++;
            }

            return properties;
        }

        private string FormatObject(object @object)
        {
            var type = @object.GetType();

            if (type.IsNumeric())
            {
                if (type == typeof(float))
                {
                    return ((float)@object).ToString(CultureInfo.InvariantCulture);
                }
                else if (type == typeof(double))
                {
                    return ((double)@object).ToString(CultureInfo.InvariantCulture);
                }
                else if (type == typeof(decimal))
                {
                    return ((decimal)@object).ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    return @object.ToString();
                }
            }
            else if (type == typeof(Guid))
            {
                _qouteTemplate[1] = @object.ToString();
                return string.Join("", _qouteTemplate, 0, 3);
            }
            else if (type == typeof(string))
            {
                _qouteTemplate[1] = EscapeCharacters(@object.ToString());
                return string.Join("", _qouteTemplate, 0, 3);
            }
            else if (type == typeof(DateTime))
            {
                _qouteTemplate[1] = ToStringReallyFast((DateTime)@object);
                return string.Join("", _qouteTemplate, 0, 3);
            }
            else
            {
                return Serialize(@object);
            }
        }

        private string[] ConvertDynamicsToStrings(object objects)
        {
            var typedArray = (object[])objects;
            var objectCount = typedArray.Length;
            var strings = new string[objectCount];

            for (int i = 0; i < objectCount; ++i)
            {
                strings[i] = FormatObject(typedArray[i]);
            }

            return strings;
        }

        private string[] ConvertObjectsToStrings(object objects)
        {
            var typedArray = (object[])objects;
            var objectCount = typedArray.Length;
            var strings = new string[objectCount];

            for (int i = 0; i < objectCount; ++i)
            {
                strings[i] = Serialize(typedArray[i]);
            }

            return strings;
        }

        private string[] ConvertPrecisionValuesToStrings<T>(object objects)
        {
            var typedArray = (T[])objects;
            var objectCount = typedArray.Length;
            var strings = new string[objectCount];

            for (int i = 0; i < objectCount; ++i)
            {
                strings[i] = typedArray[i].ToString().Replace(",",".");
            }

            return strings;
        }

        private string[] ConvertBytesAndBase64EncodeToStrings(object objects)
        {
            var typedArray = (byte[])objects;
            var strings = new string[] { Convert.ToBase64String(typedArray) };

            return strings;
        }

        private string[] ConvertValuesToStrings<T>(object objects)
        {
            var typedArray = (T[]) objects;
            var objectCount = typedArray.Length;
            var strings = new string[objectCount];

            for (int i = 0; i < objectCount; ++i)
            {
                strings[i] = typedArray[i].ToString();
            }

            return strings;
        }

        private string[] ConvertBoolsToStrings(object objects)
        {
            var typedArray = (bool[])objects;
            var objectCount = typedArray.Length;
            var strings = new string[objectCount];

            for (int i = 0; i < objectCount; ++i)
            {
                strings[i] = typedArray[i] ? "true" : "false";
            }

            return strings;
        }

        private string[] ConvertValuesToQuotedStrings<T>(object objects)
        {
            var typedArray = (T[])objects;
            var objectCount = typedArray.Length;
            var strings = new string[objectCount];

            for (int i = 0; i < objectCount; ++i)
            {
                _qouteTemplate[1] = typedArray[i].ToString();
                strings[i] = string.Join("", _qouteTemplate, 0, 3);
            }

            return strings;
        }

        private string[] ConvertDateTimeToQuotedStrings(object objects)
        {
            var typedArray = (DateTime[])objects;
            var objectCount = typedArray.Length;
            var strings = new string[objectCount];

            for (int i = 0; i < objectCount; ++i)
            {
                _qouteTemplate[1] = ToStringReallyFast(typedArray[i]);
                strings[i] = string.Join("", _qouteTemplate, 0, 3);
            }

            return strings;
        }

        private string[] QuoteStrings(object objects)
        {
            var stringsArray = (string[])objects;

            for (int i = 0; i < stringsArray.Length; ++i)
            {
                _qouteTemplate[1] = EscapeCharacters(stringsArray[i]);
                stringsArray[i] = string.Join("", _qouteTemplate, 0, 3);
            }

            return stringsArray;
        }

        private string BuildJsonArray(string[] arrayElements)
        {
            var count = arrayElements.Length + 2;
            var strArray = new string[count];

            strArray[0] = "[";
            for (var i = 1; i < count - 1; ++i)
            {
                _jsonArrayTemplate[0] = arrayElements[i - 1];
                strArray[i] = string.Join("", _jsonArrayTemplate, 0, i == count - 2 ? 1 : 2);
            }
            strArray[count - 1] = "]";

            return string.Join("", strArray, 0, count);
        }

        private string BuildJsonObject(string[][] properties)
        {
            var count = properties.Length + 2;
            var strArray = new string[count];

            strArray[0] = "{";
            for (var i = 1; i < count-1; ++i)
            {
                _jsonObjectTemplate[0] = properties[i - 1][0];
                _jsonObjectTemplate[2] = properties[i - 1][1];
                strArray[i] = string.Join("", _jsonObjectTemplate, 0, i == count - 2 ? 3 : 4);
            }
            strArray[count-1] = "}";

            return string.Join("", strArray, 0, count);
        }

        private static GetPropertiesValues CreateGetPropertiesValues(Type type)
        {
            var method = new DynamicMethod("CreateGetPropertiesFor" + type.Name, MethodAttributes.Public | MethodAttributes.Static, CallingConventions.Standard, typeof(string[][]), new[] { typeof(object), typeof(JsonSerializer), typeof(string[]) }, typeof(JsonSerializer), true);
            var ilGenerator = method.GetILGenerator();
            var typeLocal = ilGenerator.DeclareLocal(type);
            var stringMultiArray = ilGenerator.DeclareLocal(typeof(string[][]));
            var properties = type.GetProperties();
    
            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(type.IsClass ? OpCodes.Castclass : OpCodes.Unbox_Any, type);
            ilGenerator.Emit(OpCodes.Stloc, typeLocal);

            ilGenerator.Emit(OpCodes.Ldc_I4, properties.Length);
            ilGenerator.Emit(OpCodes.Newarr, typeof(string[]));
            ilGenerator.Emit(OpCodes.Stloc, stringMultiArray);

            int index = 0;
            properties.Each(property =>
            {
                ilGenerator.Emit(OpCodes.Ldloc, stringMultiArray);
                ilGenerator.Emit(OpCodes.Ldc_I4, index);
                var stringArray = AddParameter(property, ilGenerator, typeLocal);
                ilGenerator.Emit(OpCodes.Ldloc, stringArray);
                ilGenerator.Emit(OpCodes.Stelem_Ref);
                index++;
            });            

            ilGenerator.Emit(OpCodes.Ldloc, stringMultiArray);
            ilGenerator.Emit(OpCodes.Ret);
            return (GetPropertiesValues)method.CreateDelegate(typeof(GetPropertiesValues)); ;
        }

        private static LocalBuilder AddParameter(PropertyInfo propertyInfo, ILGenerator ilGenerator, LocalBuilder typeLocal)
        {
            var stringArray = ilGenerator.DeclareLocal(typeof(string[]));

            ilGenerator.Emit(OpCodes.Ldc_I4, 2);
            ilGenerator.Emit(OpCodes.Newarr, typeof(System.String));
            ilGenerator.Emit(OpCodes.Stloc, stringArray);
            ilGenerator.Emit(OpCodes.Ldloc, stringArray);
            ilGenerator.Emit(OpCodes.Ldc_I4, 0);
            ilGenerator.Emit(OpCodes.Ldstr, "\"" + propertyInfo.Name + "\"");
            ilGenerator.Emit(OpCodes.Stelem_Ref);
            ilGenerator.Emit(OpCodes.Ldloc, stringArray);
            ilGenerator.Emit(OpCodes.Ldc_I4, 1);

            if (propertyInfo.PropertyType.IsNumeric())
            {
                EmitNumericIL(propertyInfo, ilGenerator, typeLocal);
            }
            else if (propertyInfo.PropertyType.IsBoolean())
            {
                EmitBooleanIL(propertyInfo, ilGenerator, typeLocal);
            }
            else if (propertyInfo.PropertyType.IsNonNumericDataType())
            {
                EmitNonNumericDataTypesIL(propertyInfo, ilGenerator, typeLocal);
            }
            else
            {
                EmitObjectIL(propertyInfo, ilGenerator, typeLocal);
            }

            ilGenerator.Emit(OpCodes.Stelem_Ref);
            return stringArray;
        }

        private static void EmitNonNumericDataTypesIL(PropertyInfo propertyInfo, ILGenerator ilGenerator, LocalBuilder typeLocal)
        {
            var stringJoinArray = ilGenerator.DeclareLocal(typeof(string[]));
            var isNull = ilGenerator.DefineLabel();
            var @continue = ilGenerator.DefineLabel();
            var propertyStringValue = ilGenerator.DeclareLocal(typeof(string));

            if (propertyInfo.PropertyType == typeof(System.String))
            {
                GetPropertyValueIl(propertyInfo, ilGenerator, typeLocal);
                ilGenerator.Emit(OpCodes.Stloc, propertyStringValue);
                ilGenerator.Emit(OpCodes.Ldloc, propertyStringValue);
                ilGenerator.Emit(OpCodes.Brfalse_S, isNull);

                ilGenerator.Emit(OpCodes.Ldarg_1);
                ilGenerator.Emit(OpCodes.Ldloc, propertyStringValue);
                ilGenerator.Emit(OpCodes.Call, typeof(JsonSerializer).GetMethod("EscapeCharacters", new[] { typeof(string) }));
                ilGenerator.Emit(OpCodes.Stloc, propertyStringValue);            
            }

            ilGenerator.Emit(OpCodes.Ldstr, "");
            ilGenerator.Emit(OpCodes.Ldarg_2);
            ilGenerator.Emit(OpCodes.Ldc_I4, 1);

            if (propertyInfo.PropertyType == typeof(System.String))
            {
                ilGenerator.Emit(OpCodes.Ldloc, propertyStringValue);
            }
            else if (propertyInfo.PropertyType == typeof(System.DateTime))
            {
                ilGenerator.Emit(OpCodes.Ldarg_1);
                GetPropertyValueIl(propertyInfo, ilGenerator, typeLocal);
                ilGenerator.Emit(OpCodes.Call, typeof(JsonSerializer).GetMethod("ToStringReallyFast", new[] { typeof(DateTime) }));
            }
            else
            {
                EmitNumericIL(propertyInfo, ilGenerator, typeLocal);
            }

            ilGenerator.Emit(OpCodes.Stelem_Ref);
            ilGenerator.Emit(OpCodes.Ldarg_2);
            ilGenerator.Emit(OpCodes.Ldc_I4, 0);
            ilGenerator.Emit(OpCodes.Ldc_I4, 3);
            ilGenerator.Emit(OpCodes.Call, typeof(System.String).GetMethod("Join", new Type[] { typeof(string), typeof(string[]), typeof(Int32), typeof(Int32) }));

            if (propertyInfo.PropertyType == typeof(System.String))
            {
                ilGenerator.Emit(OpCodes.Br_S, @continue);
                ilGenerator.MarkLabel(isNull);
                ilGenerator.Emit(OpCodes.Ldstr, "null");
                ilGenerator.MarkLabel(@continue);
            }
        }

        private static void EmitBooleanIL(PropertyInfo propertyInfo, ILGenerator ilGenerator, LocalBuilder typeLocal)
        {
            var equal = ilGenerator.DefineLabel();
            var @continue = ilGenerator.DefineLabel();

            GetPropertyValueIl(propertyInfo, ilGenerator, typeLocal);
            ilGenerator.Emit(OpCodes.Brtrue_S, equal);
            ilGenerator.Emit(OpCodes.Ldstr, "false");
            ilGenerator.Emit(OpCodes.Br_S, @continue);
            ilGenerator.MarkLabel(equal);
            ilGenerator.Emit(OpCodes.Ldstr, "true");
            ilGenerator.MarkLabel(@continue);
        }

        private static void EmitNumericIL(PropertyInfo propertyInfo, ILGenerator ilGenerator, LocalBuilder typeLocal)
        {
            var tempVar = ilGenerator.DeclareLocal(propertyInfo.PropertyType);

            GetPropertyValueIl(propertyInfo, ilGenerator, typeLocal);
            ilGenerator.Emit(OpCodes.Stloc, tempVar);
            ilGenerator.Emit(OpCodes.Ldloca, tempVar);

            if (propertyInfo.PropertyType == typeof(System.Guid))
            {
                ilGenerator.Emit(OpCodes.Call, propertyInfo.PropertyType.GetMethod("ToString", Type.EmptyTypes));
                return;
            }

            ilGenerator.Emit(OpCodes.Call, typeof(CultureInfo).GetMethod("get_InvariantCulture", Type.EmptyTypes));
            ilGenerator.Emit(OpCodes.Call, propertyInfo.PropertyType.GetMethod("ToString", new[] { typeof(System.IFormatProvider) }));
        }

        private static void EmitObjectIL(PropertyInfo propertyInfo, ILGenerator ilGenerator, LocalBuilder typeLocal)
        {
            var isNull = ilGenerator.DefineLabel();
            var @continue = ilGenerator.DefineLabel();
            var tempVar = ilGenerator.DeclareLocal(propertyInfo.PropertyType);

            GetPropertyValueIl(propertyInfo, ilGenerator, typeLocal);
            ilGenerator.Emit(OpCodes.Stloc, tempVar);
            ilGenerator.Emit(OpCodes.Ldloc, tempVar);
            ilGenerator.Emit(OpCodes.Brfalse_S, isNull);
            ilGenerator.Emit(OpCodes.Ldarg_1, typeLocal);
            ilGenerator.Emit(OpCodes.Ldloc, tempVar);
            ilGenerator.Emit(OpCodes.Call, typeof(JsonSerializer).GetMethod("Serialize", new[] { typeof(object) }));
            ilGenerator.Emit(OpCodes.Br_S, @continue);
            ilGenerator.MarkLabel(isNull);
            ilGenerator.Emit(OpCodes.Ldstr, "null");
            ilGenerator.MarkLabel(@continue);
        }

        private static void GetPropertyValueIl(PropertyInfo propertyInfo, ILGenerator ilGenerator, LocalBuilder typeLocal)
        {
            ilGenerator.Emit(typeLocal.LocalType.IsClass ? OpCodes.Ldloc : OpCodes.Ldloca_S, typeLocal);
            ilGenerator.Emit(OpCodes.Call, propertyInfo.GetGetMethod());
        }

        public unsafe string EscapeCharacters(string str)
        {
            var arrayLength = str.Length;

            bool escaped = false;
            for (var i = 0; i < arrayLength; i++)
            {
                switch (str[i])
                {
                    case '\"':
                    case '\\':
                    case '/':
                    case '\b':
                    case '\f':
                    case '\n':
                    case '\r':
                    case '\t':
                        escaped = true;
                        break;
                }
            }

            if (!escaped)
            {
                return str;
            }

            var buffer = new StringBuilder(arrayLength);
            fixed (char* dateDatePointer = str)
            {
                char* pointer = dateDatePointer;
                for (var i = 0; i < arrayLength; i++)
                {
                    switch (*pointer)
                    {
                        case '\"':
                            buffer.Append("\\\"");
                            break;
                        case '\\':
                            buffer.Append("\\\\");
                            break;
                        case '/':
                            buffer.Append("\\/");
                            break;
                        case '\b':
                            buffer.Append("\\b");
                            break;
                        case '\f':
                            buffer.Append("\\f");
                            break;
                        case '\n':
                            buffer.Append("\\n");
                            break;
                        case '\r':
                            buffer.Append("\\r");
                            break;
                        case '\t':
                            buffer.Append("\\t");
                            break;
                        default:
                            buffer.Append(*pointer);
                            break;
                    }

                    if (i < arrayLength-1)
                    {
                        pointer++;
                    }
                }
            }
            return buffer.ToString();
        }

        public unsafe string ToStringReallyFast(DateTime datetime)
        {
            char[] dateDataArray = {     ' ', ' ', ' ', ' ', '-', ' ', ' ', '-', ' ', ' ', 'T',
                                    ' ', ' ', ':', ' ', ' ', ':', ' ', ' ', '.', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
                                    '+', ' ', ' ', ':', ' ', ' '
                              };
            fixed (char* dateDatePointer = dateDataArray)
            {
                char* pointer = dateDatePointer;
                var year = datetime.Year;
                var month = datetime.Month;
                var hour = datetime.Hour;
                var day = datetime.Day;
                var minute = datetime.Minute;
                var second = datetime.Second;
                var milliseconds = datetime.Ticks % 10000000;
                var utcOffSetHour = _utcOffSet.Hours;
                var utcOffSetMinute = _utcOffSet.Minutes;

                *pointer = (char)(year / 1000 + '0');
                pointer++;
                *pointer = (char)(year % 1000 / 100 + '0');
                pointer++;
                *pointer = (char)(year % 100 / 10 + '0');
                pointer++;
                *pointer = (char)(year % 10 + '0');
                pointer += 2;
                *pointer = (char)(month / 10 + '0');
                pointer++;
                *pointer = (char)(month % 10 + '0');
                pointer += 2;
                *pointer = (char)(day / 10 + '0');
                pointer++;
                *pointer = (char)(day % 10 + '0');
                pointer += 2;
                *pointer = (char)(hour / 10 + '0');
                pointer++;
                *pointer = (char)(hour % 10 + '0');
                pointer += 2;
                *pointer = (char)(minute / 10 + '0');
                pointer++;
                *pointer = (char)(minute % 10 + '0');
                pointer += 2;
                *pointer = (char)(second / 10 + '0');
                pointer++;
                *pointer = (char)(second % 10 + '0');
                pointer += 2;
                *pointer = (char)(milliseconds / 1000000 + '0');
                pointer++;
                *pointer = (char)(milliseconds % 1000000 / 100000 + '0');
                pointer++;
                *pointer = (char)(milliseconds % 100000 / 10000 + '0');
                pointer++;
                *pointer = (char)(milliseconds % 10000 / 1000 + '0');
                pointer++;
                *pointer = (char)(milliseconds % 1000 / 100 + '0');
                pointer++;
                *pointer = (char)(milliseconds % 100 / 10 + '0');
                pointer++;
                *pointer = (char)(milliseconds % 10 + '0');
                pointer++;
                *pointer = utcOffSetHour < 0 ? '-' : '+';
                pointer++;
                *pointer = (char)(utcOffSetHour / 10 + '0');
                pointer++;
                *pointer = (char)(utcOffSetHour % 10 + '0');
                pointer += 2;
                *pointer = (char)(utcOffSetMinute / 10 + '0');
                pointer++;
                *pointer = (char)(utcOffSetMinute % 10 + '0');
            }
            return new String(dateDataArray);
        }
    }
}
