﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace HaveBoxJSON.Converters
{
    internal class JsonDeserializer
    {
        private delegate object CreateDtoInstance(Dictionary<string, ParseDetails> objects, JsonDeserializer jsonDeserializer);
        private delegate object ArrayInstaceCreator(ParseDetails[] parseDetails, JsonDeserializer jsonDeserializer);
        private delegate object CreateDictionary(Dictionary<string, ParseDetails> objects, JsonDeserializer jsonDeserializer);
        private Dictionary<Type, CreateDtoInstance> _calculatedDtos;
        private Dictionary<Type, ArrayInstaceCreator> _calculatedArrayCreators;
        private Dictionary<Type, CreateDictionary> _dictionaryCreators;

        public JsonDeserializer()
        {
            _calculatedDtos = new Dictionary<Type, CreateDtoInstance>();
            _calculatedArrayCreators = new Dictionary<Type, ArrayInstaceCreator>();
            _dictionaryCreators = new Dictionary<Type, CreateDictionary>();
        }

        public T Deserialize<T>(string json)
        {
            return (T) Deserialize(typeof(T), json);
        }

        public object Deserialize(Type type, string json)
        {
            var startIndex = 0;
            var charArray = json.ToCharArray();

            if (type == typeof(byte[]))
            {
                return Convert.FromBase64String(ReadNextString(charArray, charArray.Length, ref startIndex));
            }
            else if (WhatComesNextOf(charArray, charArray.Length, ref startIndex, '{', '[') == '{')
            {
                var scope = ReadNextScope(charArray, charArray.Length, ref startIndex);
                if (type.HasIDictionaryInterface())
                {
                    return CreateDictionaryInstance(type, scope);
                }

                return CreateDtoWithValues(type, scope);
            }
            else
            {
                var elememts = ReadNextArray(charArray, charArray.Length, ref startIndex);

                return BuildArrayFromArrayStructure(type, elememts);
            }
        }

        public object CreateDictionaryInstance(Type type, Dictionary<string, ParseDetails> scope)
        {
            CreateDictionary createDictionaryType;
            if (!_dictionaryCreators.TryGetValue(type, out createDictionaryType))
            {
                createDictionaryType = CreateBuildDictionaryDelegate(type);
                _dictionaryCreators[type] = createDictionaryType;
            }

            return createDictionaryType(scope, this);
        }

        public object BuildArrayFromArrayStructure(Type type, ParseDetails[] elememts)
        {
            var arrayType = type;
            if (arrayType.HasIEnumerableInterface() && !arrayType.IsArray)
            {
                arrayType = arrayType.GetGenericArguments()[0].MakeArrayType();
            }

            if (arrayType == typeof(object[]))
            {
                elememts.Where(x => x.array != null).Each(x =>
                {
                    x.@string = AssignJsonArrayAsString(x.array);
                });

                elememts.Where(x => x.@object != null).Each(x =>
                {
                    x.@string = AssignJsonObjectAsString(x.@object);
                });
            }

            return CreateArray(arrayType, elememts);
        }

        private static CreateDictionary CreateBuildDictionaryDelegate(Type type)
        {
            var dictionaryType = typeof(Dictionary<,>).MakeGenericType(type.GetGenericArguments());

            var method = new DynamicMethod("CreateDictionaryA" + type.Name, MethodAttributes.Public | MethodAttributes.Static, CallingConventions.Standard, typeof(object), new[] { typeof(Dictionary<string, ParseDetails>), typeof(JsonDeserializer) }, typeof(JsonDeserializer), true);
            var ilGenerator = method.GetILGenerator();
            var dictionary = ilGenerator.DeclareLocal(dictionaryType);
            var parseDetail = ilGenerator.DeclareLocal(typeof(KeyValuePair<string, ParseDetails>));
            var parseDetailEnumerator = ilGenerator.DeclareLocal(typeof(IEnumerator<KeyValuePair<string, ParseDetails>>));

            var whileNext = ilGenerator.DefineLabel();
            var addValue = ilGenerator.DefineLabel();

            ilGenerator.Emit(OpCodes.Newobj, dictionaryType.GetConstructors().First());
            ilGenerator.Emit(OpCodes.Stloc, dictionary);
            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Call, typeof(IEnumerable<KeyValuePair<string, ParseDetails>>).GetMethod("GetEnumerator"));
            ilGenerator.Emit(OpCodes.Stloc, parseDetailEnumerator);

            ilGenerator.Emit(OpCodes.Br_S, whileNext);

            ilGenerator.MarkLabel(addValue);

            ilGenerator.Emit(OpCodes.Ldloc, parseDetailEnumerator);
            ilGenerator.Emit(OpCodes.Call, typeof(IEnumerator<KeyValuePair<string, ParseDetails>>).GetMethod("get_Current"));
            ilGenerator.Emit(OpCodes.Stloc, parseDetail);
            ilGenerator.Emit(OpCodes.Ldloc, dictionary);
            ilGenerator.Emit(OpCodes.Ldloca_S, parseDetail);
            ilGenerator.Emit(OpCodes.Call, typeof(KeyValuePair<string, ParseDetails>).GetMethod("get_Key"));

            var dataArgType = type.GetGenericArguments()[1];
            var ints = dataArgType.GetInterfaces();

            if (dataArgType.HasIDictionaryInterface())
            {
                GetValueIlForDictionary(ilGenerator, parseDetail, dataArgType);
                ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("object"));
                ilGenerator.Emit(OpCodes.Call, typeof(JsonDeserializer).GetMethod("CreateDictionaryInstance", new[] { typeof(Type), typeof(Dictionary<string, ParseDetails>) }));
            }
            else if (dataArgType.HasIEnumerableInterface() && dataArgType != typeof(string))
            {
                GetValueIlForDictionary(ilGenerator, parseDetail, dataArgType);
                ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("array"));
                ilGenerator.Emit(OpCodes.Call, typeof(JsonDeserializer).GetMethod("BuildArrayFromArrayStructure", new[] { typeof(Type), typeof(ParseDetails[]) }));
            }
            else if (!dataArgType.IsNumeric() && dataArgType != typeof(string) && dataArgType != typeof(bool) && dataArgType != typeof(Guid) && dataArgType != typeof(DateTime))
            {
                GetValueIlForDictionary(ilGenerator, parseDetail, dataArgType);
                ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("object"));
                ilGenerator.Emit(OpCodes.Call, typeof(JsonDeserializer).GetMethod("CreateDtoWithValues", new[] { typeof(Type), typeof(Dictionary<string, ParseDetails>) }));
            }
            else
            {
                HandleCsDatatypes(type, ilGenerator, parseDetail);
            }

            ilGenerator.Emit(OpCodes.Call, dictionaryType.GetMethod("Add"));

            ilGenerator.MarkLabel(whileNext);
            ilGenerator.Emit(OpCodes.Ldloc, parseDetailEnumerator);
            ilGenerator.Emit(OpCodes.Call, typeof(IEnumerator).GetMethod("MoveNext"));
            ilGenerator.Emit(OpCodes.Brtrue_S, addValue);

            ilGenerator.Emit(OpCodes.Ldloc, parseDetailEnumerator);
            ilGenerator.Emit(OpCodes.Call, typeof(IDisposable).GetMethod("Dispose"));

            ilGenerator.Emit(OpCodes.Ldloc, dictionary);
            ilGenerator.Emit(OpCodes.Ret);
            return (CreateDictionary)method.CreateDelegate(typeof(CreateDictionary));
        }

        private static void GetValueIlForDictionary(ILGenerator ilGenerator, LocalBuilder parseDetail, Type dataArgType)
        {
            ilGenerator.Emit(OpCodes.Ldarg_1);
            ilGenerator.Emit(OpCodes.Ldtoken, dataArgType);
            ilGenerator.Emit(OpCodes.Call, typeof(Type).GetMethod("GetTypeFromHandle", new[] { typeof(RuntimeTypeHandle) }));
            ilGenerator.Emit(OpCodes.Ldloca_S, parseDetail);
            ilGenerator.Emit(OpCodes.Call, typeof(KeyValuePair<string, ParseDetails>).GetMethod("get_Value"));
        }

        private static void HandleCsDatatypes(Type type, ILGenerator ilGenerator, LocalBuilder parseDetail)
        {
            ilGenerator.Emit(OpCodes.Ldloca_S, parseDetail);
            ilGenerator.Emit(OpCodes.Call, typeof(KeyValuePair<string, ParseDetails>).GetMethod("get_Value"));
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("string"));

            var dataArgType = type.GetGenericArguments()[1];

            if (dataArgType == typeof(Single) || dataArgType == typeof(Double) || dataArgType == typeof(Decimal))
            {
                ilGenerator.Emit(OpCodes.Call, typeof(CultureInfo).GetMethod("get_InvariantCulture"));
                ilGenerator.Emit(OpCodes.Call, dataArgType.GetMethod("Parse", new[] { typeof(string), typeof(IFormatProvider) }));
            }
            else if (dataArgType == typeof(DateTime) || dataArgType == typeof(Guid))
            {
                ilGenerator.Emit(OpCodes.Call, dataArgType.GetMethod("Parse", new[] { typeof(string) }));
            }
            else
            {
                ilGenerator.Emit(OpCodes.Call, typeof(Convert).GetMethod("To" + dataArgType.Name, new[] { typeof(string) }));
            }
        }

        private static string AssignJsonObjectAsString(Dictionary<string, ParseDetails> objects)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("{");
            objects.Each(x =>
            {
                stringBuilder.Append("\"");
                stringBuilder.Append(x.Key);
                stringBuilder.Append("\":");

                if (x.Value.@object != null)
                {
                    stringBuilder.Append(AssignJsonObjectAsString(x.Value.@object));
                }
                else if (!x.Value.isString)
                {
                    stringBuilder.Append(x.Value.@string);
                }
                else
                {
                    stringBuilder.Append("\"");
                    stringBuilder.Append(x.Value.@string);
                    stringBuilder.Append("\"");
                }
                stringBuilder.Append(",");
            });
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            stringBuilder.Append("}");

            return stringBuilder.ToString();
        }

        private static string AssignJsonArrayAsString(ParseDetails[] parseDetails)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("[");
            parseDetails.Each(x =>
            {
                if (x.array != null)
                {
                    stringBuilder.Append(AssignJsonArrayAsString(x.array));
                } 
                else if (!x.isString)
                {
                    stringBuilder.Append(x.@string);
                }
                else
                {
                    stringBuilder.Append("\"");
                    stringBuilder.Append(x.@string);
                    stringBuilder.Append("\"");
                }
                stringBuilder.Append(",");
            });
            stringBuilder.Remove(stringBuilder.Length-1, 1);
            stringBuilder.Append("]");

            return stringBuilder.ToString();
        }

        public object CreateArray(Type type, ParseDetails[] elememts)
        {
            ArrayInstaceCreator test;
            if (!_calculatedArrayCreators.TryGetValue(type, out test))
            {
                test = GetCreateArrayInstanceFor(type);
                _calculatedArrayCreators[type] = test;
            }
            return test(elememts, this);
        }

        private static ArrayInstaceCreator GetCreateArrayInstanceFor(Type type)
        {
            var method = new DynamicMethod("CreateArrayInstanceFor" + type.Name, MethodAttributes.Public | MethodAttributes.Static, CallingConventions.Standard, typeof(object), new[] { typeof(ParseDetails[]), typeof(JsonDeserializer) }, typeof(JsonDeserializer), true);
            var ilGenerator = method.GetILGenerator();
            var elementCount = ilGenerator.DeclareLocal(typeof(int));
            var array = ilGenerator.DeclareLocal(type);
            var i = ilGenerator.DeclareLocal(typeof(int));

            var elementType = type.GetElementType();

            var forLoop = ilGenerator.DefineLabel();
            var @return = ilGenerator.DefineLabel();

            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Ldlen);
            ilGenerator.Emit(OpCodes.Conv_I4);
            ilGenerator.Emit(OpCodes.Stloc, elementCount);
            ilGenerator.Emit(OpCodes.Ldloc, elementCount);
            ilGenerator.Emit(OpCodes.Newarr, elementType);
            ilGenerator.Emit(OpCodes.Stloc, array);
            ilGenerator.Emit(OpCodes.Ldc_I4_0);
            ilGenerator.Emit(OpCodes.Stloc, i);
            ilGenerator.Emit(OpCodes.Br, forLoop);
            ilGenerator.MarkLabel(@return);

            if (elementType == typeof(string) || elementType == typeof(object))
            {
                EmitStringArrayIl(ilGenerator, array, i);
            }
            else if (elementType == typeof(float) || elementType == typeof(double) || elementType == typeof(decimal))
            {
                EmitTryParseInvariantArrayIl(type, ilGenerator, array, i, elementType);
            }
            else if (elementType == typeof(bool))
            {
                EmitBoolArrayIl(ilGenerator, array, i);
            }
            else if (elementType.IsArray)
            {
                EmitArrayOfArrayIl(type, ilGenerator, array, i);
            }
            else if (elementType.IsNumeric() || elementType == typeof(Guid) || elementType == typeof(DateTime) || elementType == typeof(char))
            {
                EmitTryParseArrayIl(ilGenerator, array, i, elementType);
            }
            else
            {
                EmitJsonObjectArrayIl(type, ilGenerator, array, i);
            }

            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldc_I4_1);
            ilGenerator.Emit(OpCodes.Add);
            ilGenerator.Emit(OpCodes.Stloc, i);
            ilGenerator.MarkLabel(forLoop);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldloc, elementCount);
            ilGenerator.Emit(OpCodes.Blt_S, @return);
            ilGenerator.Emit(OpCodes.Ldloc, array);
            ilGenerator.Emit(OpCodes.Ret);

            return (ArrayInstaceCreator)method.CreateDelegate(typeof(ArrayInstaceCreator));
        }

        private static void EmitJsonObjectArrayIl(Type type, ILGenerator ilGenerator, LocalBuilder array, LocalBuilder i)
        {
            ilGenerator.Emit(OpCodes.Ldloc, array);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldarg_1);

            ilGenerator.Emit(OpCodes.Ldtoken, type.GetElementType());
            ilGenerator.Emit(OpCodes.Call, typeof(Type).GetMethod("GetTypeFromHandle", new[] { typeof(System.RuntimeTypeHandle) }));

            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldelem_Ref);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("object"));
            ilGenerator.Emit(OpCodes.Call, typeof(JsonDeserializer).GetMethod("CreateDtoWithValues", new[] { typeof(System.Type), typeof(Dictionary<string, ParseDetails>) }));
            ilGenerator.Emit(OpCodes.Castclass, type.GetElementType());
            ilGenerator.Emit(OpCodes.Stelem_Ref);
        }

        private static void EmitArrayOfArrayIl(Type type, ILGenerator ilGenerator, LocalBuilder array, LocalBuilder i)
        {
            ilGenerator.Emit(OpCodes.Ldloc, array);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldarg_1);

            ilGenerator.Emit(OpCodes.Ldtoken, type.GetElementType());
            ilGenerator.Emit(OpCodes.Call, typeof(Type).GetMethod("GetTypeFromHandle", new[] { typeof(System.RuntimeTypeHandle) }));

            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldelem_Ref);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("array"));
            ilGenerator.Emit(OpCodes.Call, typeof(JsonDeserializer).GetMethod("CreateArray", new[] { typeof(System.Type), typeof(ParseDetails[]) }));
            ilGenerator.Emit(OpCodes.Castclass, type.GetElementType());
            ilGenerator.Emit(OpCodes.Stelem_Ref);
        }

        private static void EmitBoolArrayIl(ILGenerator ilGenerator, LocalBuilder array, LocalBuilder i)
        {
            var isTrue = ilGenerator.DefineLabel();
            var isFalse = ilGenerator.DefineLabel();

            ilGenerator.Emit(OpCodes.Ldloc, array);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldelem_Ref);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("booleanValue"));

            ilGenerator.Emit(OpCodes.Brtrue_S, isTrue);
            ilGenerator.Emit(OpCodes.Ldc_I4_0);
            ilGenerator.Emit(OpCodes.Br_S, isFalse);
            ilGenerator.MarkLabel(isTrue);
            ilGenerator.Emit(OpCodes.Ldc_I4_1);
            ilGenerator.MarkLabel(isFalse);
            ilGenerator.Emit(OpCodes.Stelem_I1);
        }

        private static void EmitTryParseArrayIl(ILGenerator ilGenerator, LocalBuilder array, LocalBuilder i, Type elementType)
        {
            var tempVar = ilGenerator.DeclareLocal(elementType);
            var isFalse = ilGenerator.DefineLabel();

            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldelem_Ref);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("string"));
            ilGenerator.Emit(OpCodes.Ldloca, tempVar);
            ilGenerator.Emit(OpCodes.Call, elementType.GetMethod("TryParse", new[] { typeof(string), tempVar.LocalType.MakeByRefType() }));
            ilGenerator.Emit(OpCodes.Brfalse_S, isFalse);
            ilGenerator.Emit(OpCodes.Ldloc, array);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldloc, tempVar);

            var localType = tempVar.LocalType;

            if (localType == typeof(sbyte))
            {
                ilGenerator.Emit(OpCodes.Stelem_I1);
            }
            else if (localType == typeof(short) || localType == typeof(ushort))
            {
                ilGenerator.Emit(OpCodes.Stelem_I2);
            }
            else if (localType == typeof(long) || localType == typeof(ulong))
            {
                ilGenerator.Emit(OpCodes.Stelem_I8);
            }
            else if (localType == typeof(int) || localType == typeof(uint))
            {
                ilGenerator.Emit(OpCodes.Stelem_I4);
            }
            else
            {
                ilGenerator.Emit(OpCodes.Pop);
                ilGenerator.Emit(OpCodes.Ldelema, tempVar.LocalType);
                ilGenerator.Emit(OpCodes.Ldloc, tempVar);
                ilGenerator.Emit(OpCodes.Stobj, tempVar.LocalType);
            }

            ilGenerator.MarkLabel(isFalse);
        }

        private static void EmitTryParseInvariantArrayIl(Type type, ILGenerator ilGenerator, LocalBuilder array, LocalBuilder i, Type elementType)
        {
            var tempVar = ilGenerator.DeclareLocal(elementType);
            var isFalse = ilGenerator.DefineLabel();

            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldelem_Ref);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("string"));

            ilGenerator.Emit(OpCodes.Ldc_I4, 0x1ff);
            ilGenerator.Emit(OpCodes.Call, typeof(CultureInfo).GetMethod("get_InvariantCulture"));

            ilGenerator.Emit(OpCodes.Ldloca, tempVar);
            ilGenerator.Emit(OpCodes.Call, tempVar.LocalType.GetMethod("TryParse", new[] { typeof(string), typeof(NumberStyles), typeof(IFormatProvider), tempVar.LocalType.MakeByRefType() }));
            ilGenerator.Emit(OpCodes.Brfalse_S, isFalse);
            ilGenerator.Emit(OpCodes.Ldloc, array);
            ilGenerator.Emit(OpCodes.Ldloc, i);

            if (type.GetElementType() == typeof(float))
            {
                ilGenerator.Emit(OpCodes.Ldloc, tempVar);
                ilGenerator.Emit(OpCodes.Stelem_R4);
            }
            else if (type.GetElementType() == typeof(double))
            {
                ilGenerator.Emit(OpCodes.Ldloc, tempVar);
                ilGenerator.Emit(OpCodes.Stelem_R8);
            }
            else
            {
                ilGenerator.Emit(OpCodes.Ldelema, tempVar.LocalType);
                ilGenerator.Emit(OpCodes.Ldloc, tempVar);
                ilGenerator.Emit(OpCodes.Stobj, tempVar.LocalType);
            }

            ilGenerator.MarkLabel(isFalse);
        }

        private static void EmitStringArrayIl(ILGenerator ilGenerator, LocalBuilder array, LocalBuilder i)
        {
            ilGenerator.Emit(OpCodes.Ldloc, array);
            ilGenerator.Emit(OpCodes.Ldloc, i);

            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Ldloc, i);
            ilGenerator.Emit(OpCodes.Ldelem_Ref);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("string"));

            ilGenerator.Emit(OpCodes.Stelem_Ref);
        }

        private static ParseDetails[] ReadNextArray(Char[] charArray, int charArrayCount, ref int startIndex)
        {
            IList<ParseDetails> objects = new List<ParseDetails>();

            for (; ; )
            {
                var nextValue = ReadNextValue(charArray, charArrayCount, ref startIndex, ']');
                objects.Add(nextValue);

                if (WhatComesNextOf(charArray, charArrayCount, ref startIndex, ',', ']') == ']')
                {
                    startIndex++;
                    break;
                }
            }

            return objects.ToArray();
        }

        public object CreateDtoWithValues(Type type, Dictionary<string, ParseDetails> scope)
        {
            CreateDtoInstance createDtoInstance;
            if (!_calculatedDtos.TryGetValue(type, out createDtoInstance))
            {
                createDtoInstance = CreateDtoInstanceDelegate(type);
                _calculatedDtos[type] = createDtoInstance;
            }
            return createDtoInstance(scope, this);
        }

        private static CreateDtoInstance CreateDtoInstanceDelegate(Type type)
        {
            var method = new DynamicMethod("CreateDtoInstanceFor" + type.Name, MethodAttributes.Public | MethodAttributes.Static, CallingConventions.Standard, typeof(object), new[] { typeof(Dictionary<string, ParseDetails>), typeof(JsonDeserializer) }, typeof(JsonDeserializer), true);
            var ilGenerator = method.GetILGenerator();
            var dtoInstance = ilGenerator.DeclareLocal(type);
            var tempVariable = ilGenerator.DeclareLocal(typeof(ParseDetails));
            var properties = type.GetProperties();

            ilGenerator.Emit(OpCodes.Newobj, type.GetConstructor(Type.EmptyTypes));

            ilGenerator.Emit(OpCodes.Stloc, dtoInstance);

            properties.Each(property => {
                var noValueLabel = ilGenerator.DefineLabel();

                ilGenerator.Emit(OpCodes.Ldarg_0);
                ilGenerator.Emit(OpCodes.Ldstr, property.Name);
                ilGenerator.Emit(OpCodes.Ldloca, tempVariable);
                ilGenerator.Emit(OpCodes.Call, typeof(Dictionary<string, ParseDetails>).GetMethod("TryGetValue"));
                ilGenerator.Emit(OpCodes.Brfalse_S, noValueLabel);

                var propertyType = property.PropertyType;
                if (propertyType == typeof(System.Guid) || propertyType == typeof(System.DateTime) || propertyType.IsNumeric())
                {
                    EmitSetterWithTryParseIl(ilGenerator, dtoInstance, tempVariable, property);
                }
                else if (propertyType == typeof(bool))
                {
                    EmitBooleanSetterIl(ilGenerator, dtoInstance, tempVariable, property, "booleanValue");
                }
                else if (propertyType == typeof(string))
                {
                    EmitBooleanSetterIl(ilGenerator, dtoInstance, tempVariable, property, "string");
                }
                else if (propertyType.HasIDictionaryInterface() || propertyType.IsIDictionaryInterface())
                {
                    EmitJDictionaryIl(ilGenerator, dtoInstance, tempVariable, property);
                }
                else if (propertyType.HasIEnumerableInterface())
                {
                    EmitJSONArrayIl(ilGenerator, dtoInstance, tempVariable, property);
                }
                else
                {
                    EmitObjectSetterIl(ilGenerator, dtoInstance, tempVariable, property);
                }

                ilGenerator.MarkLabel(noValueLabel);
            });

            ilGenerator.Emit(OpCodes.Ldloc, dtoInstance);
            ilGenerator.Emit(OpCodes.Ret);

            return (CreateDtoInstance)method.CreateDelegate(typeof(CreateDtoInstance));
        }

        private static void EmitJDictionaryIl(ILGenerator ilGenerator, LocalBuilder dtoInstance, LocalBuilder tempVariable, PropertyInfo property)
        {
            PrepareObjectIL(ilGenerator, dtoInstance, tempVariable, property);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("object"));
            ilGenerator.Emit(OpCodes.Call, typeof(JsonDeserializer).GetMethod("CreateDictionaryInstance", new[] { typeof(Type), typeof(Dictionary<string, ParseDetails>) }));
            ilGenerator.Emit(OpCodes.Call, property.GetSetMethod());
        }

        private static void PrepareObjectIL(ILGenerator ilGenerator, LocalBuilder dtoInstance, LocalBuilder tempVariable, PropertyInfo property)
        {
            ilGenerator.Emit(OpCodes.Ldloc, dtoInstance);
            ilGenerator.Emit(OpCodes.Ldarg_1);
            ilGenerator.Emit(OpCodes.Ldtoken, property.PropertyType);
            ilGenerator.Emit(OpCodes.Call, typeof(Type).GetMethod("GetTypeFromHandle", new[] { typeof(System.RuntimeTypeHandle) }));
            ilGenerator.Emit(OpCodes.Ldloc, tempVariable);
        }

        private static void EmitJSONArrayIl(ILGenerator ilGenerator, LocalBuilder dtoInstance, LocalBuilder tempVariable, PropertyInfo property)
        {
            PrepareObjectIL(ilGenerator, dtoInstance, tempVariable, property);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("array"));
            ilGenerator.Emit(OpCodes.Call, typeof(JsonDeserializer).GetMethod("BuildArrayFromArrayStructure", new[] { typeof(Type), typeof(ParseDetails[]) }));
            ilGenerator.Emit(OpCodes.Call, property.GetSetMethod());
        }

        private static void EmitObjectSetterIl(ILGenerator ilGenerator, LocalBuilder dtoInstance, LocalBuilder tempVariable, PropertyInfo property)
        {
            PrepareObjectIL(ilGenerator, dtoInstance, tempVariable, property);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("object"));
            ilGenerator.Emit(OpCodes.Call, typeof(JsonDeserializer).GetMethod("CreateDtoWithValues", new[] { typeof(Type), typeof(Dictionary<string, ParseDetails>) }));
            ilGenerator.Emit(OpCodes.Call, property.GetSetMethod());
        }

        private static void EmitBooleanSetterIl(ILGenerator ilGenerator, LocalBuilder dtoInstance, LocalBuilder tempVariable, PropertyInfo property, string parseDetailField)
        {
            ilGenerator.Emit(OpCodes.Ldloc, dtoInstance);
            ilGenerator.Emit(OpCodes.Ldloc, tempVariable);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField(parseDetailField));
            ilGenerator.Emit(OpCodes.Call, property.GetSetMethod());
        }

        private static void EmitStringSetterIl(ILGenerator ilGenerator, LocalBuilder dtoInstance, LocalBuilder tempVariable, PropertyInfo property)
        {
            ilGenerator.Emit(OpCodes.Ldloc, dtoInstance);
            ilGenerator.Emit(OpCodes.Ldloc, tempVariable);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("string"));
            ilGenerator.Emit(OpCodes.Call, property.GetSetMethod());
        }

        private static void EmitSetterWithTryParseIl(ILGenerator ilGenerator, LocalBuilder dtoInstance, LocalBuilder tempVariable, PropertyInfo property)
        {
            var tempVar = ilGenerator.DeclareLocal(property.PropertyType);
            ilGenerator.Emit(OpCodes.Ldloc, tempVariable);
            ilGenerator.Emit(OpCodes.Ldfld, typeof(ParseDetails).GetField("string"));

            if (property.PropertyType == typeof(decimal))
            {
                ilGenerator.Emit(OpCodes.Ldc_I4, 0x1ff);
                ilGenerator.Emit(OpCodes.Call, typeof(CultureInfo).GetMethod("get_InvariantCulture"));
                ilGenerator.Emit(OpCodes.Ldloca, tempVar);
                ilGenerator.Emit(OpCodes.Call, property.PropertyType.GetMethod("TryParse", new[] { typeof(string), typeof(NumberStyles), typeof(IFormatProvider), property.PropertyType.MakeByRefType() }));
            }
            else
            {
                ilGenerator.Emit(OpCodes.Ldloca, tempVar);
                ilGenerator.Emit(OpCodes.Call, property.PropertyType.GetMethod("TryParse", new[] { typeof(string), property.PropertyType.MakeByRefType() }));
            }

            ilGenerator.Emit(OpCodes.Pop);
            ilGenerator.Emit(OpCodes.Ldloc, dtoInstance);
            ilGenerator.Emit(OpCodes.Ldloc, tempVar);
            ilGenerator.Emit(OpCodes.Call, property.GetSetMethod());
        }

        private static Dictionary<string, ParseDetails> ReadNextScope(Char[] charArray, int charArrayCount, ref int startIndex)
        {
            Dictionary<string, ParseDetails> objects = new Dictionary<string, ParseDetails>();

            GetIndexForNextToken(charArray, charArrayCount, ref startIndex, '{');
            for (; ; )
            {
                var objectName = ReadNextString(charArray, charArrayCount, ref startIndex);

                GetIndexForNextToken(charArray, charArrayCount, ref startIndex, ':');
                var nextValue = ReadNextValue(charArray, charArrayCount, ref startIndex, '}');
                if (!nextValue.isNull)
                {
                    objects.Add(objectName, nextValue);
                }

                if (WhatComesNextOf(charArray, charArrayCount, ref startIndex, ',', '}') == '}')
                {
                    startIndex++;
                    break;
                }
            }

            return objects;
        }

        private static ParseDetails ReadNextValue(Char[] charArray, int charArrayCount, ref int startIndex, char terminalChar)
        {
            startIndex++;
            GetIndexForNextNonWhiteSpace(charArray, charArrayCount, ref startIndex);
            
            switch(charArray[startIndex])
            {
                case '\"':
                    return new ParseDetails { @string = ReadNextString(charArray, charArrayCount, ref startIndex), isString = true };
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '-':
                    return new ParseDetails { @string = ReadNextNumericValue(charArray, charArrayCount, ref startIndex) };
                case 'T':
                case 't':
                    WhatComesNextOf(charArray, charArrayCount, ref startIndex, ',', terminalChar);
                    return new ParseDetails { booleanValue = true, @string = "true" };
                case 'F':
                case 'f':
                    WhatComesNextOf(charArray, charArrayCount, ref startIndex, ',', terminalChar);
                    return new ParseDetails { booleanValue = false, @string = "false" };
                case 'N':
                case 'n':
                    WhatComesNextOf(charArray, charArrayCount, ref startIndex, ',', terminalChar);
                    return new ParseDetails { isNull = true, @string = "null" };
                case '{':                    
                    return new ParseDetails { @object = ReadNextScope(charArray, charArrayCount, ref startIndex) };
                case '[':
                    return new ParseDetails 
                    { 
                        array = ReadNextArray(charArray, charArrayCount, ref startIndex),
                    };
                default:
                    throw new NotSupportedException();
            }
        }

        private static string ReadNextString(Char[] charArray, int charArrayCount, ref int startIndex)
        {         
            var stringStartIndex = 0;
            bool escaped;

            GetIndexForNextToken(charArray, charArrayCount, ref startIndex, '\"');
            startIndex++;
            stringStartIndex = startIndex;
            GetIndexForNextTokenAndCheckForEscape(charArray, charArrayCount, ref startIndex, '\"', out escaped);

            return escaped ? DeescapeString(charArray, stringStartIndex, startIndex) : new string(charArray, stringStartIndex, startIndex - stringStartIndex);
        }

        private static string DeescapeString(Char[] charArray, int startIndex, int endIndex)
        {
            var buffer = new char[endIndex-startIndex];
            var index = startIndex;
            var bufferindex = 0;

            for (; ; )
            {
                var @char = charArray[index++];

                if (@char == '\"')
                {
                    break;
                }

                if (@char != '\\')
                {
                    buffer[bufferindex++] = @char;
                    continue;
                }

                @char = charArray[index++];

                switch (@char)
                {
                    case '\"':
                        buffer[bufferindex++] = '\"';
                        continue;
                    case '\\':
                        buffer[bufferindex++] = '\\';
                        continue;
                    case '/':
                        buffer[bufferindex++] = '/';
                        continue;
                    case 'b':
                        buffer[bufferindex++] = '\b';
                        continue;
                    case 'f':
                        buffer[bufferindex++] = '\f';
                        continue;
                    case 'r':
                        buffer[bufferindex++] = '\r';
                        continue;
                    case 't':
                        buffer[bufferindex++] = '\t';
                        continue;
                    case 'n':
                        buffer[bufferindex++] = '\n';
                        continue;
                    case 'u':
                        var hexbuffer = new char[4];

                        hexbuffer[0] = charArray[index++];
                        hexbuffer[1] = charArray[index++];
                        hexbuffer[2] = charArray[index++];
                        hexbuffer[3] = charArray[index++];

                        buffer[bufferindex++] = (char)Convert.ToInt32(new string(hexbuffer), 16);
                        continue;
                }
            }

            return new string(buffer, 0, bufferindex);
        }

        private static string ReadNextNumericValue(Char[] charArray, int charArrayCount, ref int startIndex)
        {
            var stringStartIndex = 0;
            GetIndexForNextNumericToken(charArray, charArrayCount, ref startIndex);
            stringStartIndex = startIndex;
            GetIndexForNextNonNumericToken(charArray, charArrayCount, ref startIndex);
            return new string(charArray, stringStartIndex, startIndex - stringStartIndex);
        }

        private static void GetIndexForNextNumericToken(Char[] charArray, int charArrayCount, ref int startIndex)
        {
            for (; startIndex < charArrayCount; ++startIndex)
            {
                if ((charArray[startIndex] >= 48 && charArray[startIndex] <= 57) || charArray[startIndex] == 46 || charArray[startIndex] == '-')
                {
                    return;
                }
            }

            throw new IndexOutOfRangeException();
        }

        private static void GetIndexForNextNonNumericToken(Char[] charArray, int charArrayCount, ref int startIndex)
        {
            for (; startIndex < charArrayCount; ++startIndex)
            {
                if ((charArray[startIndex] < 48 || charArray[startIndex] > 57) && charArray[startIndex] != 46 && charArray[startIndex] != '-')
                {
                    return;
                }
            }

            throw new IndexOutOfRangeException();
        }

        private static void GetIndexForNextNonWhiteSpace(Char[] charArray, int charArrayCount, ref int startIndex)
        {
            for (; startIndex < charArrayCount; ++startIndex)
            {
                if (charArray[startIndex] != ' ')
                {
                    return;
                }
            }

            throw new IndexOutOfRangeException();
        }

        private static void GetIndexForNextToken(Char[] charArray, int charArrayCount, ref int startIndex, char token)
        {
            for (; startIndex < charArrayCount; ++startIndex)
            {
                if (charArray[startIndex] == token)
                {
                    return;
                }
            }

            throw new IndexOutOfRangeException();
        }


        private static void GetIndexForNextTokenAndCheckForEscape(Char[] charArray, int charArrayCount, ref int startIndex, char token, out bool hittedEscaped)
        {
            hittedEscaped = false;

            for (; startIndex < charArrayCount; ++startIndex)
            {
                if (charArray[startIndex] == '\\')
                {
                    hittedEscaped = true;
                }

                if (charArray[startIndex] == token)
                {
                    if (token != '\"' || (token == '\"' && startIndex > 0 && charArray[startIndex - 1] != '\\') || startIndex == 0)
                    {
                        return;
                    }
                }
            }

            throw new IndexOutOfRangeException();
        }

        private static char WhatComesNextOf(Char[] charArray, int charArrayCount, ref int startIndex, char token1, char token2)
        {
            for (; startIndex < charArrayCount; ++startIndex)
            {
                if (charArray[startIndex] == token1)
                {
                    return token1;
                }

                if (charArray[startIndex] == token2)
                {
                    return token2;
                }
            }

            throw new IndexOutOfRangeException();
        }

        public class ParseDetails
        {
            public Dictionary<string, ParseDetails> @object;
            public ParseDetails[] array;
            public bool isString;
            public string @string;
            public bool booleanValue;
            public bool isNull;
        }
    }
}
